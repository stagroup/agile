<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TaskDuration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table('projects')
            ->addColumn('start_date', 'date', ['null' => true, 'default' => null, 'after' => 'name'])
            ->addColumn('due_date', 'date', ['null' => true, 'default' => null, 'after' => 'start_date'])
            ->update();
        $this->table('tasks')
            ->addColumn('start_date', 'date', ['null' => true, 'default' => null, 'after' => 'priority'])
            ->addColumn('due_date', 'date', ['null' => true, 'default' => null, 'after' => 'start_date'])
            ->update();
    }
}
