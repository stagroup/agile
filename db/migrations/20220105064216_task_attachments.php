<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TaskAttachments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table('task_files', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'string', ['length' => 20])
            ->addColumn('task_id', 'integer')
            ->addColumn('filename', 'string', ['length' => 256])
            ->addColumn('description', 'string', ['length' => 256])
            ->addColumn('created_by', 'integer')
            ->addColumn('created_at', 'datetime')
            ->create();
    }
}
