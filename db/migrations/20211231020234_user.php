<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table('users')
            ->addColumn('username', 'string', ['length' => 100])
            ->addColumn('password', 'string', ['length' => 100])
            ->addColumn('fullname', 'string', ['length' => 100])
            ->addColumn('is_dev', 'boolean')
            ->addColumn('is_client', 'boolean')
            ->addColumn('is_admin', 'boolean')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->addColumn('deleted_at', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }
}
