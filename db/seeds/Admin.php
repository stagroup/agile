<?php


use Phinx\Seed\AbstractSeed;
use models\User;

class Admin extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        if (User::where('username', 'admin')->first())
            return;
        User::create([
            'username' => 'admin@agile.local',
            'fullname' => 'Administrator',
            'password' => 'admin123',
            'is_admin' => true,
            'is_dev' => true,
            'is_client' => true
        ]);
    }
}
