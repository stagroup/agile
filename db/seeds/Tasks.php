<?php


use Phinx\Seed\AbstractSeed;
use models\User;
use models\Project;
use models\Task;

class Tasks extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $project = Project::create([
            'name' => 'Lorem ipsum',
            'created_by' => 1
        ]);
        $priorities = [
            Task::PRIORITY_URGENT,
            Task::PRIORITY_HIGH,
            Task::PRIORITY_NORMAL,
            Task::PRIORITY_LOW
        ];
        for($i = 1; $i <= 5; $i++) {
            Task::create([
                'project_id' => $project->id,
                'name' => 'task 00'.$i,
                'description' => 'task descriptions',
                'priority' => $priorities[mt_rand(0,3)],
                'created_by' => 1
            ]);
        }
    }
}
