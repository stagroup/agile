<?php


use Phinx\Seed\AbstractSeed;

class Users extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        foreach (['user1@agile.local','user2@agile.local','user3@agile.local','user4@agile.local'] as $username) {
            if (!$user = models\User::where('username', $username)->first()) {
                models\User::create([
                    'username' => $username,
                    'fullname' => $username,
                    'password' => $username.'123',
                    'is_dev' => mt_rand(0,1),
                    'is_client' => mt_rand(0,1),
                    'is_admin' => mt_rand(0,1)
                ]);
            }
        }
    }
}
