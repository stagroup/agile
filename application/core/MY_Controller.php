<?php

class MY_Controller extends CI_Controller
{
	public function _remap($function, $params = array()) {
		if (is_cli()) {
			return call_user_func_array([$this, $function], $params);
		}
		$req_method = $this->input->method(false);
		$rest_function = $req_method.'_'.$function;
		$this->pre_remap($function, $params);

		if (method_exists($this, $rest_function)) {
			call_user_func_array([$this, $rest_function], $params);
			return;
		}
		else if ($req_method == 'get' && method_exists($this, $function)) {
			$this->function = $function;
			call_user_func_array([$this, $function], $params);
			return;
		}
		$this->show_404();
	}

	protected function show_404() {
		show_404();
	}

	protected function pre_remap($function, $params) {}

	protected function request($key = null) {
		return request($key);
	}

	protected function view($view, $data = []) {
		$env = new SimpleTemplateEngine\Environment(rtrim(VIEWPATH, DIRECTORY_SEPARATOR), '.php');
		if (isset($this->session) && $this->session->user)
			$data['me'] =& $this->session->user;
		echo $env->render($view, $data);
	}

	protected function json($data, $http_code = 200) {
		$this->output->set_header('Content-type: application/json')
			->set_output(json_encode($data))
			->set_status_header($http_code)
			->_display();
		exit();
	}

	protected function halt($http_code = 200, $message = '') {
		if ($message && $message instanceof Exception)
			$message = $message->getMessage();
		
		if (is_string($message))
			$this->output->set_content_type('text/html');
		else {
			$message = json_encode($message);
			$this->output->set_content_type('application/json');
		}

		$this->output->set_status_header($http_code, $message)
			->set_output($message)
			->_display();
		exit();
	}
}

class AuthController extends MY_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	protected function me() {
		return $this->session->auth;
	}

	protected function pre_remap($function, $params) {
		if (!($user = $this->session->auth)) {
			if ($this->input->is_ajax_request()) {
				$this->output->set_status_header(401)
					->_display();
				exit;
			}
			else {
				redirect('auth/login?ref='.htmlentities($this->uri->uri_string()));
				return;
			}
		}
		
		if (!$this->auth()) {
			return $this->show_401();
		}
	}

	protected function show_401() {
		show_error('Unauthorized access<br><a href="'.site_url('/').'">Back</a>', 401);
	}

	protected function auth() {
		return true;
	}
}