<?php
use \Illuminate\Database\Capsule\Manager as Capsule;

require __DIR__.'/database.php';
$config = [];
$capsule = new Capsule;
$capsule->addConnection([
    'driver'	=> $db['default']['dbdriver'] ?: 'mysql',
    'host' 		=> $db['default']['hostname'] ?: 'localhost',
	'username'	=> $db['default']['username'] ?: 'root',
	'password'	=> $db['default']['password'] ?: '',
	'database'	=> $db['default']['database'],
	'prefix'	=> $db['default']['dbprefix'],
	'options'   => [
		\PDO::ATTR_EMULATE_PREPARES => true
	],
	'strict'	=> $db['default']['stricton'] === true
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

if (!function_exists('query_logs')) {
	function query_logs(callable $query) {
		Capsule::enableQueryLog();
		$query();
		$logs = Capsule::getQueryLog();
		Capsule::disableQueryLog();
		return $logs;
	}
}