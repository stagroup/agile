<?php

static $pusher;

function pushnotify($channel, $action, $data = null) {
	global $pusher;

	if (!$conf = config_item('pusher'))
		return false;

	if (!$pusher) {
		$pusher = new Pusher\Pusher( 
			$conf['key'], 
			$conf['secret'], 
			$conf['app_id'], 
			array('cluster' => $conf['cluster']) 
		);
	}
	return $pusher->trigger( $channel, $action, $data );
}