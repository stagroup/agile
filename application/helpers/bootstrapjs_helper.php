<?php

function bs_modal_lg_start($title, $selector) {
	$id = $selector[0] == '#' ? substr($selector, 1) : null;
	$class = $selector[0] == '.' ? substr($selector, 1) : null;
	return '
		<div '.($id ? 'id="'.$id.'"' : '').' class="modal fade '.($class ?: '').'" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title">'.$title.'</h4>
                </div>
                <div class="modal-body">
	';
}

function bs_modal_lg_end($btn_OK = 'OK', $btn_cancel = 'Cancel') {
	if ($btn_OK)
		is_string($btn_OK) OR $btn_OK = 'OK';
	if ($btn_cancel)
		is_string($btn_cancel) OR $btn_cancel = 'Cancel';
	return '
				</div>
                <div class="modal-footer">
                	'.($btn_cancel ? '<button type="button" class="btn btn-default cancel" data-dismiss="modal">'.$btn_cancel.'</button>' : '').'
                  	'.($btn_OK ? '<button type="button" class="btn btn-primary ok">'.$btn_OK.'</button>' : '').'
                </div>

              </div>
            </div>
          </div>
	';
}