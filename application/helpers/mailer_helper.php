<?php

function send_email($from, $to, $subject, $message = '', $is_html = false) {
	if (func_num_args() == 4) {
		$is_html = !!$message;
	}
	if (func_num_args() == 3 || func_num_args() == 4) {
		$message = $subject;
		$subject = $to;
		$to = $from;
		$from = null;
	}

	$ci =& get_instance();
	$ci->load->library('email');
	$conf = config_item('mailer');
	$mail_props = [
		'protocol'		=> 'smtp',
		'priority'		=> 1,
		'smtp_host'		=> $conf['smtp_host'],
		'smtp_crypto'	=> $conf['ssl'] ? 'ssl' : '',
		'smtp_port'		=> $conf['smtp_port'],
		'smtp_user'		=> $system_email_user = $conf['username'],
		'smtp_pass'		=> $conf['password'],
	];
	if ($is_html)
		$mail_props['mailtype'] = 'html';
	$ci->email->initialize($mail_props);

	if ($from) {
		$from = (array) $from;
		if (count($from) <= 1) {
			$from[] = $from[0];
		}
	}
	else {
		$from = [$system_email_user, $conf['name'] ?: $system_email_user];
	}

	$ci->email->from($from[0], $from[1]);
	$ci->email->to($to);
	// $ci->email->cc('another@another-example.com');
	// $ci->email->bcc('them@their-example.com');

	$ci->email->subject($subject);
	$ci->email->message($message);

	$log_file = APPPATH.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'mailerlog_'.date('Ymd').'.log';
	if ($res = $ci->email->send()) {
		file_put_contents($log_file, "Sent to $to".PHP_EOL.$subject.PHP_EOL.PHP_EOL, FILE_APPEND);
	}
	else {
		file_put_contents($log_file, $ci->email->print_debugger().PHP_EOL.PHP_EOL, FILE_APPEND);
	}
	return $res;
}

/*function send_html_email($from, $to, $subject, $message = '') {
	if (func_num_args() == 3) {
		$message = $subject;
		$subject = $to;
		$to = $from;
		$from = null;
	}

	$ci =& get_instance();
	$ci->load->library('email');
	$ci->email->initialize([
		'mailtype'		=> 'html',
		'protocol'		=> getenv('MAILER_PROTOCOL'),
		'priority'		=> 1,
		'smtp_host'		=> getenv('MAILER_SMTP_HOST'),
		'smtp_crypto'	=> getenv('MAILER_SSL') ? 'ssl' : '',
		'smtp_port'		=> getenv('MAILER_SMTP_PORT'),
		'smtp_user'		=> $system_email_user = getenv('SYSTEM_EMAIL_USER'),
		'smtp_pass'		=> getenv('SYSTEM_EMAIL_PASSWORD'),
	]);

	if ($from) {
		$from = (array) $from;
		if (count($from) <= 1) {
			$from[] = $from[0];
		}
	}
	else {
		$from = [$system_email_user, getenv('SYSTEM_EMAIL_SENDER') ?: $system_email_user];
	}

	$ci->email->from($from[0], $from[1]);
	$ci->email->to($to);
	// $ci->email->cc('another@another-example.com');
	// $ci->email->bcc('them@their-example.com');

	$ci->email->subject($subject);
	$ci->email->message($message);

	return $ci->email->send();
}*/