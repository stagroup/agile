<?php

class BootstrapTable
{
	public $total = 0, $rows = [];
	private $query, $search, $searchFields = [],
			$sort, $sortDir, $customSort = false,
			$offset, $limit;

	public function __construct($query) {
		if (is_string($query))
			$query = $query::query();
		$this->query = $query;

		$request = request();
		$this->search = isset($request['search']) ? $request['search'] : null;
		$this->sort = isset($request['sort']) ? $request['sort'] : null;
		$this->sortDir = isset($request['order']) ? $request['order'] : null;
		$this->offset = isset($request['offset']) ? $request['offset'] : 0;
		$this->limit = isset($request['limit']) ? $request['limit'] : 0;

		return $this;
	}

	public static function make($query) {
		return new self($query);
	}

	public function searchFields($fields) {
		$this->searchFields = $fields;
		return $this;
	}

	public function query(callable $cb) {
		$cb($this->query, $this->search);
		return $this;
	}

	public function sort(callable $cb) {
		if ($this->sort && $this->sortDir)
			$cb($this->query, $this->sort, $this->sortDir);
		$this->customSort = true;
		return $this;
	}

	public function json() {
		$this->build();
		header('Content-type: application/json');
		echo json_encode($this);
	}

	public function response(callable $cb) {
		$this->build($cb);
		return $this;
	}

	private function build(callable $cb = null) {
		if ($this->search && $this->searchFields) {
			$this->query->where(function($query) {
				$search = str_replace(' ', '%', $this->search);
				foreach ((array)$this->searchFields as $field) {
					if (is_callable($field)) {
						$query->orWhere(function($query) use (&$field, $search) {
							$field($query, $search);
						});
					}
					else {
						$query->orWhere($field, 'LIKE', "%$search%");
					}
				}
			});
		}

		if (!$this->customSort && $this->sort && $this->sortDir)
			$this->query->orderBy($this->sort, $this->sortDir);

		$total = $this->query->count();
		$rows = $this->query
					->limit($this->limit)->offset($this->offset)
					->get();
		$this->rows = $cb ?
						$rows->map($cb) :
						$rows->each(function(&$row, $index) {
							$row->_rowNumber = $index + $this->offset + 1;
						});
		if ($rows->count() > 0) {
			$this->total = $total;
		}
	}
}