<?php

static $json_requests = [];

function request($key = null) {
	$ci =& get_instance();
	switch ($ci->input->method(true)) {
		case 'GET':
			return $ci->input->get($key);
			break;
		case 'POST':
			return $ci->input->post($key);
			break;
		default:
			if (isset($_SERVER['CONTENT_TYPE']) && stripos($_SERVER['CONTENT_TYPE'], 'application/json') !== false)
				return is_null($key) ? $ci->raw_input_stream : $ci->input->input_stream($key);
			break;
	}
}

static $view_env;

function view($view, $data = []) {
	global $view_env;
	if (!$view_env)
		$view_env = new SimpleTemplateEngine\Environment(rtrim(VIEWPATH, DIRECTORY_SEPARATOR), '.php');
	$ci =& get_instance();
	if (isset($ci->session) && $ci->session->user)
		$data['me'] =& $ci->session->user;
	echo $view_env->render($view, $data);
}

function month_name($date) {
	return ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'][date('n', $date)];
}

function json($data) {
	header('Content-type: application/json');
	echo json_encode($data);
}

function debug($data) {
	ob_clean();
	header('Content-type: text/plain');
	print_r($data);
	exit;
}