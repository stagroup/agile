<?php

use models\Task as DBTask;
use models\Project as DBProject;

class Task extends AuthController
{
	public function index() {
		redirect('task/list');
	}

	public function list() {
		if ($this->input->is_ajax_request()) {
			$this->json(
				DBTask::with([
					'project', 
					'creator' => function($u) { $u->select('id', 'fullname'); }, 
					'updater' => function($u) { $u->select('id', 'fullname'); }
				])->where(function($q) {
					if ($project_id = request('project_id'))
						$q->where('project_id', $project_id);
					if ($search = request('search')) {
						$q->where(function($q) use ($search) {
							$q->where('name', 'LIKE', "%$search%")
								->orWhere('description', 'LIKE', "%$search%");
						});
					}
					$q->whereIn('priority', explode(',', request('priority') ?: ''));
					
				})->get()
			);
		}
		else {
			view('task_list', [
				'projects' => models\Project::orderBy('name')->get()
			]);
		}
	}

	public function detail($id) {
		$task = DBTask::with(['project', 'creator', 'updater'])->find($id);
		if ($this->input->is_ajax_request()) {
			$this->json($task);
		}
		else {
			view('task_detail', ['task' => $task]);
		}
	}

	public function projects() {
		$this->json(
			DBProject::orderBy('name')->get()
		);
	}

	public function post_add() {
		$project = null;
		$validator = $this->form_validation
						->set_rules('project_id', 'Project', [
							['valid', function($project_id) use (&$project) {
								return $project_id == '*' || ($project = DBProject::find($project_id));
							}]
						], ['valid' => 'Invalid project'])
						->set_rules('project_name', 'Project name', [
							['required', function($project_name) {
								return request('project_id') != '*' || trim($project_name);
							}]
						], ['required' => 'Project name is required'])
						->set_rules('name', 'Task name', [
							'required'
						])
						->set_rules('description', 'Description', [
							'required'
						])
						->set_rules('priority', 'Priority', 'in_list['.implode(',', DBTask::PRIORITIES).']');
		if (!$validator->run())
			return $this->halt(422, $validator->error_array());

		if (!$project) {
			$project = DBProject::create([
				'name' => trim(request('project_name')),
				'created_by' => $this->me()->id
			]);
		}

		$task = new DBTask();
		$task->project()->associate($project);
		$task->name = request('name');
		$task->description = request('description');
		$task->priority = request('priority');
		$task->creator()->associate($this->me());
		$task->save();
	}

	public function post_update($id) {
		if (!$user = DBUser::find($id))
			return $this->halt(422, 'Data not found');

		$validator = $this->form_validation
						->set_rules('username', 'Username', [
							'required',
							'valid_email',
							['unique', function($val) use (&$user) {
								return DBUser::where('username', $val)->where('id', '!=', $user->id)->count() == 0;
							}]
						], ['unique' => 'Username already exists'])
						->set_rules('fullname', 'Name', [
							'required',
							'min_length[5]'
						])
						->set_rules('password', 'Password', [
							['optional', function($pwd) {
								return $pwd === '' || strlen($pwd) >= DBUser::PASSWORD_MINLENGTH;
							}]
						], ['optional' => '{field} min. length is {param}']);
		if (!$validator->run())
			return $this->halt(422, $validator->error_array());

		$user->username = request('username');
		$user->fullname = request('fullname');
		$user->password = request('password');
		$user->is_dev = !!request('is_dev');
		$user->is_client = !!request('is_client');
		$user->is_admin = !!request('is_admin');
		$user->save();
	}
}
