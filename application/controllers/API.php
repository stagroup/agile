<?php
use models\EmailJob;

class API extends CI_Controller
{
	public function emailInputData($data) {
		$emails = EmailJob::where('data', $data)->get();
		if ($emails->count() == 0)
			return;
		$this->load->helper('mailer');
		foreach ($emails as $email) {
			foreach ($email->recipients()->whereNull('sent_at')->get() as $recipient) {
				if (send_email($recipient->email, $email->subject, $email->message, true)) {
					$recipient->sent_at = new DateTime();
					$recipient->save();
				}
			}
			if (!$email->recipients()->whereNull('sent_at')->exists()) {
				$email->all_sent = true;
				$email->save();
			}
		}
	}

	// public function email
}