<?php

use models\User;
use models\UserGroup;
use models\GroupAccess;
use \Illuminate\Database\Capsule\Manager as Capsule;

class Usermgmt extends AuthController
{
	public function index() {
		view('users', [
			'me' => $this->session->auth,
			'groups' => UserGroup::orderBy('name')->get()
		]);
	}

	public function btUsers() {
		BootstrapTable::make(
			User::with(['group'])
		)
		->searchFields(['username', 'name'])
		->query(function($q) {
			if ($group_id = request('group_id'))
				$q->where('group_id', $group_id);
		})
		->json();
	}

	public function tambah() {
		view('user_tambah', [
			'groups' => UserGroup::orderBy('name')->get()
		]);
	}

	public function post_tambah() {
		$group = null;
		$validator = $this->form_validation
						->set_rules('username', 'Username', [
							'required',
							'min_length[5]',
							['unique', function($val) {
								return User::where('username', $val)->count() == 0;
							}]
						], [
							'unique' => 'Username sudah ada',
							'min_length' => 'Panjang username min. 5'
						])
						->set_rules('name', 'Nama Lengkap', [
							'required',
							'min_length[5]'
						], ['min_length' => 'Panjang nama min. 5'])
						->set_rules('password', 'Password', [
							'required',
							'min_length[5]'
						], ['min_length' => 'Panjang password min. 5'])
						->set_rules('email', 'E-mail', [
							'valid_email'
						], ['valid_email' => '{field} invalid'])
						->set_rules('group_id', 'Grup User', [
							['valid_group', function($id) use (&$group) {
								return $group = UserGroup::find($id);
							}]
						], ['valid_group' => 'Grup User invalid']);
		if (!$validator->run())
			return $this->tambah();

		$user = new User();
		$user->username = request('username');
		$user->name = request('name');
		$user->password = request('password');
		$user->email = request('email') ?: null;
		$user->group()->associate($group);
		$user->save();
		redirect('usermgmt');
	}

	public function edit($id) {
		if (!$user = User::find($id))
			return show_error('User invalid');

		view('user_edit', [
			'user' => $user,
			'groups' => UserGroup::orderBy('name')->get()
		]);
	}

	public function post_edit($id) {
		if (!$user = User::find($id))
			return show_error('User invalid');

		$group = null;
		$validator = $this->form_validation
						->set_rules('username', 'Username', [
							'required',
							'min_length[5]',
							['unique', function($val) use ($id) {
								return User::where('id', '!=', $id)->where('username', $val)->count() == 0;
							}]
						], [
							'unique' => 'Username sudah ada',
							'min_length' => 'Panjang username min. 5'
						])
						->set_rules('name', 'Nama Lengkap', [
							'required',
							'min_length[5]'
						], ['min_length' => 'Panjang nama min. 5'])
						->set_rules('email', 'E-mail', [
							'valid_email'
						], ['valid_email' => '{field} invalid'])
						->set_rules('group_id', 'Grup User', [
							['valid_group', function($id) use (&$group) {
								return $group = UserGroup::find($id);
							}]
						], ['valid_group' => 'Grup User invalid']);
		if (!$validator->run())
			return $this->tambah();

		$user->username = request('username');
		$user->name = request('name');
		if ($new_password = request('password'))
			$user->password = $new_password;
		$user->email = request('email') ?: null;
		$user->group()->associate($group);
		$user->save();
		redirect('usermgmt');
	}

	public function groups() {
		view('usergroups');
	}

	public function btGroups() {
		BootstrapTable::make(
			UserGroup::class
		)
		->searchFields(['name'])
		->json();
	}

	public function tambahGroup() {
		view('usergroup_tambah', [
			'all_access' => GroupAccess::GROUP
		]);
	}

	public function post_tambahGroup() {
		$validator = $this->form_validation
						->set_rules('name', 'Nama Grup', [
							'required',
							['unique_group', function($val) {
								return UserGroup::where('name', $val)->count() == 0;
							}]
						], ['unique_group' => 'Nama grup sudah ada']);
		if (!$validator->run())
			return $this->tambahGroup();

		try {
			Capsule::transaction(function() {
				$group = new UserGroup();
				$group->name = request('name');
				$group->save();

				if ($access = (request('access') ?: [])) {
					$group->access()->saveMany(array_map(function($access_name) {
						return new GroupAccess(['name' => $access_name]);
					}, $access));
				}
			});
			redirect('usermgmt/groups');
		} catch (Exception $e) {
			show_error($e->getMessage());
		}
	}

	public function editGroup($id) {
		if (!$group = UserGroup::with('access')->find($id))
			return show_error('Grup invalid');

		view('usergroup_edit', [
			'data' => $group,
			'all_access' => GroupAccess::GROUP,
			'has_all_access' => $group->hasAllAccess()
		]);
	}

	public function post_editGroup($id) {
		if (!$group = UserGroup::find($id))
			return show_error('Grup invalid');

		$validator = $this->form_validation
						->set_rules('name', 'Nama Grup', [
							'required',
							['unique_group', function($val) use ($id) {
								return UserGroup::where('id', '!=', $id)->where('name', $val)->count() == 0;
							}]
						], ['unique_group' => 'Nama grup sudah ada']);
		if (!$validator->run())
			return $this->editGroup($id);

		try {
			Capsule::transaction(function() use (&$group) {
				$group->name = request('name');
				$group->save();

				$group->access()->delete();
				if ($access = (request('access') ?: [])) {
					$group->access()->saveMany(array_map(function($access_name) {
						return new GroupAccess(['name' => $access_name]);
					}, $access));
				}
			});
			redirect('usermgmt/groups');
		} catch (Exception $e) {
			show_error($e->getMessage());
		}
	}

	public function detailGroup($id) {
		if (!$group = UserGroup::find($id))
			return show_error('Grup invalid');

		$group_access = $group->access->pluck('name')->toArray();
		foreach (array_filter(array_keys(GroupAccess::GROUP), function($g) use ($group_access) {
			return array_intersect($group_access, GroupAccess::GROUP[$g]);
		}) as $g) {
			$all_access[$g] = array_filter(GroupAccess::GROUP[$g], function($access) use ($group_access) {
				return in_array($access, $group_access);
			});
		}

		view('usergroup_detail', [
			'data' => $group,
			'all_access' => $all_access
		]);
	}

	public function post_deleteGroup($id) {
		User::where('group_id', $id)->update(['group_id' => null]);
		UserGroup::where('id', $id)->delete();
		redirect('usermgmt/groups');
	}

	protected function auth() {
		return $this->session->auth->isMaster();
	}

	public function __construct() {
		parent::__construct();
		$this->load->helper('bootstraptable');
		$this->load->library('Form_validation');
	}
}
