<?php

class Home extends AuthController
{
	public function index() {
		view('home');
	}

	public function changepassword() {
		$this->load->library('Form_validation');
		view('changepassword', [
			'success' => $this->session->flashdata('changepassword') ?: false
		]);
	}

	public function post_changepassword() {
		$me =& $this->session->auth;
		$this->load->library('Form_validation');
		$validator = $this->form_validation
						->set_rules('old_password', 'Password', [
							'required',
							['valid_password', function($val) use ($me) {
								return $me->validatePassword($val);
							}]
						], ['valid_password' => 'Password salah'])
						->set_rules('new_password', 'Password baru', [
							'required',
							'min_length[5]'
						], ['min_length' => 'Panjang password min. 5'])
						->set_rules('confirm_password', 'Password baru', [
							'matches[new_password]'
						], ['matches' => 'Password baru belum dikonfirmasi dengan benar']);
		if (!$validator->run())
			return $this->changepassword();

		$me->update(['password' => request('new_password')]);
		$this->session->set_flashdata('changepassword', true);
		redirect('changepassword');
	}

	public function profile() {
		$this->load->library('Form_validation');
		view('user_profile', [
			'success' => $this->session->flashdata('profile') ?: false
		]);
	}

	public function post_profile() {
		$me =& $this->session->auth;
		$this->load->library('Form_validation');
		$validator = $this->form_validation
						->set_rules('new_password', 'Password baru', [
							'required',
							'min_length[5]'
						], ['min_length' => 'Panjang password min. 5'])
						->set_rules('confirm_password', 'Password baru', [
							'matches[new_password]'
						], ['matches' => 'Password baru belum dikonfirmasi dengan benar']);
		if (!$validator->run())
			return $this->profile();

		$me->update([
			'name' => request('name'),
			'email' => request('email')
		]);
		$this->session->set_flashdata('profile', true);
		redirect('profile');
	}
}
