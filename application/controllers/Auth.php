<?php

use models\User;

class Auth extends MY_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function login() {
		view('login');
	}

	public function post_login() {
		$user = null;
		if ($this->form_validation
				->set_rules('username', 'Username',
					[
						'required',
						'valid_email',
						['valid_user', function($val) use (&$user) {
							return !$val || !!($user = User::where('username', $val)->first());
						}]
					],
					['valid_user' => 'Invalid username']
				)
				->set_rules('password', 'Password',
					[
						'required',
						['correct_password', function($val) use (&$user) {
							return !$user || $user->validatePassword($val);
						}]
					],
					['correct_password' => 'Wrong password']
				)
				->run()
		) {
			$this->session->auth = $user;
			redirect(isset($_GET['ref']) ? $_GET['ref'] : '');
		}
		else {
			$this->login();
		}
	}

	public function logout() {
		$this->session->auth = null;
		redirect('auth/login');
	}
}
