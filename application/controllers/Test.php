<?php
class Test extends AuthController
{
	public function index() {
		$this->json(
			\Carbon\Carbon::create('2022-01-01')->lessThan(\Carbon\Carbon::now())
		);
	}
}
