<?php

use models\User as DBUser;

class Admin extends AuthController
{
	public function usermgmt() {
		if ($this->input->is_ajax_request()) {
			$this->json(
				DBUser::where(function($q) {
					$auth = explode(',', request('auth'));
					$q->where(function($q) use ($auth) {
						foreach (DBUser::ALL_AUTH as $field => $title) {
							if (in_array($field, $auth))
								$q->orWhere($field, 1);
						}
					});
				})->orderBy('fullname')->get()
			);
		}
		else
			view('user_list');
	}

	public function user($id) {
		$this->json(DBUser::find($id));
	}

	public function post_addUser() {
		$validator = $this->form_validation
						->set_rules('username', 'Username', [
							'required',
							'valid_email',
							['unique', function($val) {
								return DBUser::where('username', $val)->count() == 0;
							}]
						], ['unique' => 'Username already exists'])
						->set_rules('fullname', 'Name', [
							'required',
							'min_length[5]'
						])
						->set_rules('password', 'Password', [
							'required',
							'min_length['.DBUser::PASSWORD_MINLENGTH.']'
						]);
		if (!$validator->run())
			return $this->halt(422, $validator->error_array());

		$user = new DBUser();
		$user->username = request('username');
		$user->fullname = request('fullname');
		$user->password = request('password');
		$user->is_dev = !!request('is_dev');
		$user->is_client = !!request('is_client');
		$user->is_admin = !!request('is_admin');
		$user->save();
	}

	public function post_updateUser($id) {
		if (!$user = DBUser::find($id))
			return $this->halt(422, 'Data not found');

		$validator = $this->form_validation
						->set_rules('username', 'Username', [
							'required',
							'valid_email',
							['unique', function($val) use (&$user) {
								return DBUser::where('username', $val)->where('id', '!=', $user->id)->count() == 0;
							}]
						], ['unique' => 'Username already exists'])
						->set_rules('fullname', 'Name', [
							'required',
							'min_length[5]'
						])
						->set_rules('password', 'Password', [
							['optional', function($pwd) {
								return $pwd === '' || strlen($pwd) >= DBUser::PASSWORD_MINLENGTH;
							}]
						], ['optional' => '{field} min. length is {param}']);
		if (!$validator->run())
			return $this->halt(422, $validator->error_array());

		$user->username = request('username');
		$user->fullname = request('fullname');
		$user->password = request('password');
		$user->is_dev = !!request('is_dev');
		$user->is_client = !!request('is_client');
		$user->is_admin = !!request('is_admin');
		$user->save();
	}

	protected function auth() {
		return $this->session->auth->checkAuth('admin');
	}
}
