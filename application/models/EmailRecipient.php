<?php
namespace models;

class EmailRecipient extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'email_recipients',
			$primaryKey = 'id',
			$guarded = [],
			$casts = [
				'sent_at' => 'date'
			];
	public $timestamps = false;
	
	public function job() {
		return $this->belongsTo(EmailJob::class, 'job_id');
	}
}