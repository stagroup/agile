<?php
namespace models;

class User extends \Illuminate\Database\Eloquent\Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'users',
			$primaryKey = 'id',
			$guarded = [],
			$hidden = ['password'],
			$appends = [
				'all_auth'
			],
			$casts = [
				'is_dev' => 'boolean',
				'is_client' => 'boolean',
				'is_admin' => 'boolean'
			];
	const PASSWORD_MINLENGTH = 5;
	const ALL_AUTH = [
		'is_dev' => 'developer',
		'is_client' => 'client',
		'is_admin' => 'admin'
	];

    public function setPasswordAttribute($pwd) {
        $this->attributes['password'] = password_hash($pwd, PASSWORD_BCRYPT);
    }

	public function validatePassword($pwd) {
        return password_verify($pwd, $this->password);
    }

    public function getAllAuthAttribute() {
    	return array_values(array_map(
    		function($field) { return self::ALL_AUTH[$field]; },
    		array_filter(array_keys(self::ALL_AUTH), function($field) { return $this->$field; })
    	));
    }

    public function checkAuth($type) {
    	if (is_array($type))
    		return count(array_intersect(array_filter($this->all_auth), $type)) > 0;
    	else
    		return in_array($type, $this->all_auth);
    }
}