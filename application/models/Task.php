<?php
namespace models;

class Task extends \Illuminate\Database\Eloquent\Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'tasks',
			$primaryKey = 'id',
			$guarded = [],
			$casts = [
				'start_date' => 'date',
				'due_date' => 'date'
			],
			$appends = [
				'is_overdue',
				'due'
			];
	const PRIORITY_URGENT = 'urgent',
			PRIORITY_HIGH = 'high',
			PRIORITY_NORMAL = 'normal',
			PRIORITY_LOW = 'low';
	const PRIORITIES = [
		self::PRIORITY_URGENT,
		self::PRIORITY_HIGH,
		self::PRIORITY_NORMAL,
		self::PRIORITY_LOW
	];
	const PRIORITY_COLORS = [
		self::PRIORITY_URGENT => '#FF0000',
		self::PRIORITY_HIGH => '#FFA500',
		self::PRIORITY_NORMAL => '#000',
		self::PRIORITY_LOW => '#808080'
	];

	public function files() {
		return $this->hasMany(TaskFile::class, 'task_id');
	}

	public function project() {
		return $this->belongsTo(Project::class, 'project_id');
	}

	public function creator() {
		return $this->belongsTo(User::class, 'created_by');
	}

	public function updater() {
		return $this->belongsTo(User::class, 'updated_by');
	}

	public function getColorAttribute() {
		if (!array_key_exists($this->priority, self::PRIORITY_COLORS))
			return '#C0C0C0';
		return self::PRIORITY_COLORS[$this->priority];
	}

	public function getDueAttribute() {
		if (!$this->due_date)
			return null;
		if ($years = $this->due_date->diffInYears(null, false))
			return ($years < 0 ? 'in ' : '').abs($years).' year'.(abs($years) > 1 ? 's' : '');
		if ($months = $this->due_date->diffInMonths(null, false))
			return ($months < 0 ? 'in ' : '').abs($months).' month'.(abs($months) > 1 ? 's' : '');
		if ($weeks = $this->due_date->diffInWeeks(null, false))
			return ($weeks < 0 ? 'in ' : '').abs($weeks).' week'.(abs($weeks) > 1 ? 's' : '');
		$days = $this->due_date->diffInDays(null, false);
		return $days == 0 ? 'today' : ($days < 0 ? 'in ' : '').abs($days).' day'.(abs($days) > 1 ? 's' : '');
	}

	public function getIsOverdueAttribute() {
		return $this->due_date && $this->due_date->lessThan(\Carbon\Carbon::now());
	}
}