<?php
namespace models;

class Project extends \Illuminate\Database\Eloquent\Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'projects',
			$primaryKey = 'id',
			$guarded = [],
			$casts = [
				'start_date' => 'date',
				'due_date' => 'date'
			],
			$appends = ['is_overdue'];

	public function creator() {
		return $this->belongsTo(User::class, 'created_by');
	}

	public function tasks() {
		return $this->hasMany(Task::class);
	}

	public function getIsOverdueAttribute() {
		return $this->due_date && $this->due_date->lessThan(\Carbon\Carbon::now());
	}
}