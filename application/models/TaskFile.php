<?php
namespace models;

class TaskFile extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'projects',
			$primaryKey = 'id',
			$guarded = [],
			$casts = ['id' => 'string'];
	const UPDATED_AT = null;
	public $incrementing = false;
}