<?php
namespace models;

class EmailJob extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'email_jobs',
			$primaryKey = 'id',
			$guarded = [],
			$casts = [
				'html' => 'boolean',
				'all_sent' => 'boolean'
			];

	public function recipients() {
		return $this->hasMany(EmailRecipient::class, 'job_id');
	}
}