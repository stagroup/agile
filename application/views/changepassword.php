<?php $this->extend('layout') ?>
<?php $this->block('title', 'Akun Saya') ?>
<?php $this->block('subtitle', 'Ganti Password') ?>

<?php $this->block('content') ?>
<?php if ($success): ?>
	<div class="alert alert-success" role="alert">
		Password sudah diganti
	</div>
<?php else: ?>
	<form method="post" action="" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off">
		<div class="form-group <?=($err = form_error('old_password')) ? 'bad' : ''?>">
			<label class="control-label col-md-3">Password Sekarang
			</label>
			<div class="col-md-5">
				<input name="old_password" value="<?=set_value('old_password')?>" type="password" required="" class="form-control col-md-7">
				<?php if ($err): ?>
					<span class="text-danger text-small"><?=$err?></span>
				<?php endif ?>
			</div>
		</div>
		<div class="form-group <?=($err = form_error('new_password')) ? 'bad' : ''?>">
			<label class="control-label col-md-3">Password Baru
			</label>
			<div class="col-md-5">
				<input name="new_password" value="<?=set_value('new_password')?>" type="password" required="" class="form-control col-md-7">
				<?php if ($err): ?>
					<span class="text-danger text-small"><?=$err?></span>
				<?php endif ?>
			</div>
		</div>
		<div class="form-group <?=($err = form_error('confirm_password')) ? 'bad' : ''?>">
			<label class="control-label col-md-3">Konfirmasi Password
			</label>
			<div class="col-md-5">
				<input name="confirm_password" value="<?=set_value('confirm_password')?>" type="password" required="" class="form-control col-md-7">
				<?php if ($err): ?>
					<span class="text-danger text-small"><?=$err?></span>
				<?php endif ?>
			</div>
		</div>
		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-offset-3">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="<?=site_url('/')?>" class="btn btn-default">Cancel</a>
			</div>
		</div>
	</form>
<?php endif ?>
<?php $this->endblock() ?>