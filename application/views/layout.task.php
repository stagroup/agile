<?php $ci =& get_instance() ?>
<?php $this->extend('layout') ?>
<?php $this->block('title', "Tasks") ?>

<?php $this->block('content') ?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="<?=($is_kanban = ($ci->uri->segment(2) == 'kanban')) ? 'active' : ''?>"><a href="<?=site_url('task/kanban')?>">Kanban</a></li>
        <li class="<?=($is_list = ($ci->uri->segment(2) == 'list')) ? 'active' : ''?>"><a href="<?=site_url('task/list')?>">List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane <?=$is_kanban ? 'active' : ''?>">
        	<?=$is_kanban ? $this['content'] : ''?>
        </div>
        <div class="tab-pane <?=$is_list ? 'active' : ''?>">
        	<?=$is_list ? $this['content'] : ''?>
        </div>
    </div>
</div>
<?php $this->endblock() ?>

<?php $this->block('script') ?>
<?=$this['script']?>
<?php $this->endblock() ?>

<?php $this->block('head') ?>
<?=$this['head']?>
<?php $this->endblock() ?>