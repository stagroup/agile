<?php $ci =& get_instance() ?>
<?php $this->extend('layout.task') ?>

<?php $this->block('content') ?>
<div id="content" class="row">
	<div v-if="showList" :class="listClass">
		<div class="box box-solid">
			<div class="box-header with-border">
				<div class="form-inline pull-left">
					<button type="button" @click="form()" class="btn btn-flat btn-success">Add</button>
					<div class="input-group">
						<input type="text" v-model="filter.search" class="form-control" placeholder="Search">
						<span class="input-group-btn">
							<button type="button" @click="refreshList" class="btn btn-flat btn-primary">Search</button>
						</span>
					</div>
				</div>

				<div class="form-inline pull-right">
					<?php foreach (models\Task::PRIORITIES as $p): ?>
						<label class="checkbox-inline filter"><input type="checkbox" name="priority[]" v-model="filterPriorities.<?=$p?>"> <?=$p?></label>
					<?php endforeach ?>
					<select name="project" class="form-control" v-model="filter.project_id" style="margin-left: 10px;">
						<option value="">-All projects-</option>
						<option v-for="p in projects" :value="p.id">{{ p.name }}</option>
					</select>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="text-muted" class="narrow">#</th>
							<th class="narrow">ID</th>
							<th>Name</th>
							<th class="narrow">Priority</th>
							<th>Project</th>
							<th>Description</th>
							<th class="narrow">Due Date</th>
							<th class="narrow">Last Update</th>
							<th class="narrow"></th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(task,i) in list">
							<td class="text-muted narrow">{{ i+1 }}</td>
							<td class="narrow"><a @click.prevent="detail(task.id)" href="'<?=site_url('task')?>/'+task.id">{{ task.id }}</a></td>
							<td><a @click.prevent="detail(task.id)" :href="'<?=site_url('task')?>/'+task.id" style="display: block">{{ task.name }}</a></td>
							<td :class="'narrow priority-'+task.priority">{{ task.priority }}</td>
							<td>{{ task.project.name }}</td>
							<td>{{ task.description }}</td>
							<td :class="'narrow'+(task.is_overdue ? ' text-danger' : '')">
								<div v-if="task.due_date">
									{{ moment(task.due_date).format('D-M-Y') }}
									<div class="small">
										<i v-if="task.is_overdue && task.due != 'today'" class="fa fa-exclamation-triangle"></i>
										<i v-if="task.due == 'today'" class="fa fa-clock-o"></i>
										{{ task.due }}
									</div>
								</div>
							</td>
							<td class="narrow" style="line-height: 100%;">
								{{ moment(task.updated_at).format('D-M-Y HH:mm') }}
								<div class="small">{{ task.updater.fullname }}</div>
							</td>
							<td class="narrow" style="padding-left: 10px; padding-right: 10px;">
								<a href="javascript:;" @click="form(task.id)">Edit</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div v-if="showForm" class="col-md-5">
		<form id="formTask" method="post" :action="formAction" class="box box-success">
			<div class="box-header with-border">
				<h4 class="box-title" v-html="formTitle"></h4>
			</div>
			<div class="box-body">
				<div :class="inputClass.project">
					<label>Project</label>
					<select name="project_id" class="form-control" v-model="formData.project_id">
						<option value="">-Select project-</option>
						<option value="*">-New project-</option>
						<option v-for="p in projects" :value="p.id">{{ p.name }}</option>
					</select>
					<span class="help-block">{{ formValidation.project }}</span>
				</div>
				<div v-if="addProject" :class="inputClass.project_name">
					<label>Project name</label>
					<input type="text" name="project_name" class="form-control" v-model="formData.project_name">
					<span class="help-block">{{ formValidation.project_name }}</span>
				</div>
				<div :class="inputClass.priority">
					<?php foreach (models\Task::PRIORITIES as $p): ?>
						<label class="radio-inline"><input type="radio" name="priority" v-model="formData.priority" value="<?=$p?>"> <?=$p?></label>
					<?php endforeach ?>
					<span class="help-block">{{ formValidation.priority }}</span>
				</div>
				<div :class="inputClass.name">
					<label>Task name</label>
					<input type="text" name="name" class="form-control" v-model="formData.name">
					<span class="help-block">{{ formValidation.name }}</span>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Start</label>
						<input type="text" name="start_date" class="form-control" autocomplete="off">
						<span class="help-block">{{ formValidation.start_date }}</span>
					</div>
					<div class="form-group col-md-6">
						<label>Due</label>
						<input type="text" name="due_date" class="form-control" autocomplete="off">
						<span class="help-block">{{ formValidation.due_date }}</span>
					</div>
				</div>
				<div :class="inputClass.description">
					<label>Description</label>
					<textarea name="description" v-model="formData.description" class="form-control" rows="7"></textarea>
					<span class="help-block">{{ formValidation.description }}</span>
				</div>
				<div class="text-danger" v-if="formErr" v-html="formErr"></div>
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-flat btn-primary" @click="save()">Save</button>
				<button type="button" class="btn btn-flat btn-default pull-right" @click="closeForm()">Close</button>
			</div>
		</form>
	</div>
	<div v-if="showDetail" class="col-md-12 box box-primary">
		<div class="box-header with-border">
			<h4 class="box-title">Task #{{ task.id }}</h4>
		</div>
		<div class="box-body">
			<h4>{{ task.name }}</h4>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-flat btn-default pull-right" @click="closeDetail()">Close</button>
		</div>
	</div>
</div>
<?php $this->endblock() ?>

<?php $this->block('script') ?>
<script type="text/javascript" src="<?=base_url('assets/vue.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/jquery.form.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bower_components/moment/min/moment-with-locales.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript">
    moment.locale('id');

	new Vue({
		el: '#content',
		data: {
			list: [],
			filter: {
				project_id: '',
				search: '',
				priority: "<?=implode(',', models\Task::PRIORITIES)?>"
			},
			allPriorities: true,
			filterPriorities: {<?=implode(',', array_map(function($p) { return $p.':true'; }, models\Task::PRIORITIES))?>},
			formData: {},
			formValidation: {},
			formErr: null,
			projects: [],
			showDetail: false,
			task: {}
		},
		watch: {
			"filter.project_id": function() {
				this.refreshList();
			},
			<?php foreach (models\Task::PRIORITIES as $p): ?>
				"filterPriorities.<?=$p?>": function() {
					this.filter.priority = Object.keys(this.filterPriorities).filter(function(p) {
						return this.filterPriorities[p];
					}.bind(this)).join(',');
					this.refreshList();
				},
			<?php endforeach ?>
		},
		computed: {
			showList: function() {
				return !this.showDetail;
			},
			showForm: function() {
				return Object.keys(this.formData).indexOf('id') >= 0;
			},
			listClass: function() {
				return this.showForm ? 'col-md-7' : (this.showDetail ? 'hide' : 'col-md-12');
			},
			formTitle: function() {
				return this.formData.id ? 'Edit task' : 'New task';
			},
			formAction: function() {
				return this.formData.id ? '<?=site_url('task/update')?>/' + this.formData.id : '<?=site_url('task/add')?>';
			},
			inputClass: function() {
				return {
					name: 'form-group' + (this.formValidation.name ? ' has-error' : ''),
					description: 'form-group' + (this.formValidation.description ? ' has-error' : ''),
					priority: 'form-group' + (this.formValidation.priority ? ' has-error' : ''),
					project_id: 'form-group' + (this.formValidation.project_id ? ' has-error' : ''),
					project_name: 'form-group' + (this.formValidation.project_name ? ' has-error' : ''),
				};
			},
			addProject: function() {
				return this.formData.project_id == '*';
			}
		},
		methods: {
			refreshList: function() {
				$.getJSON('', this.filter, function(res) {
					this.list = res;
				}.bind(this));
				this.closeForm();
			},
			refreshProjects: function(then) {
				$.getJSON('<?=site_url('task/projects')?>', function(res) {
					this.projects = res;
					if (then)
						then();
				}.bind(this));
			},
			form: function(id) {
				this.formData = {id: id};
				this.refreshProjects(function() {
					$('form input[name=start_date], form input[name=due_date]').datepicker({
						format: 'd-m-yyyy'
					});
					if (id) {
						$.getJSON('<?=site_url('task')?>/detail/' + id, function(res) {
							this.formData = res;
							$('form input[name=start_date]').datepicker('update', res.start_date ? new Date(res.start_date) : null);
							$('form input[name=due_date]').datepicker('update', res.due_date ? new Date(res.due_date) : null);
						}.bind(this));
					}
					else {
						this.formData = {id: id, project_id: ''};
					}
				}.bind(this));
			},
			save: function() {
				this.formValidation = {};
				this.formErr = null;
				$('#formTask').ajaxSubmit({
					success: function(res) {
						this.closeForm();
						this.refreshList();
						this.refreshProjects();
					}.bind(this),
					error: function(xhr, status, statusText) {
						if (xhr.responseJSON) {
							this.formValidation = xhr.responseJSON;
						}
						else {
							this.formErr = 'Error: ' + xhr.status + ' ' + statusText;
						}
						console.log(arguments);
					}.bind(this)
				});
			},
			closeForm: function() {
				this.formData = {};
				this.formValidation = {};
				this.formErr = null;
			},
			detail: function(id) {
				this.closeForm();
				this.showDetail = true;
				$.getJSON('<?=site_url('task')?>/detail/' + id, function(res) {
					this.task = res;
				}.bind(this));
			},
			closeDetail: function() {
				this.showDetail = false;
				this.task = {};
			}
		},
		created: function() {
			this.refreshList();
			this.refreshProjects();
		}
	});
</script>
<?php $this->endblock() ?>

<?php $this->block('head') ?>
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>">
<style type="text/css">
	.filter + .filter {
		margin-left: 0;
	}
</style>
<?php $this->endblock() ?>