<?php
$ci =& get_instance();
$me =& $ci->session->auth;
?>

<!DOCTYPE html>
<html>
<head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title><?=$this['title']?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <style type="text/css">
        .overlay 
        {
            position: absolute; /* Sit on top of the page content */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }
        td.narrow, th.narrow {
          width: 1px;
          white-space: nowrap;
        }
        #navProjects .dropdown-menu 
        {
            width: auto;
        }
        #navProjects .menu>li>a>h4, #navProjects .menu>li>a>h4+p 
        {
            margin-left: 0;
        }
        <?php foreach (models\Task::PRIORITY_COLORS as $priority => $color): ?>
          .priority-<?=$priority?> {
            color: <?=$color?>;
          }
        <?php endforeach ?>
        .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
          margin-left: -16px;
        }
    </style>
    <?=$this['head']?>
  </head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?=site_url()?>" class="navbar-brand" style="font-weight:bold">AGILE</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <?php if ($me->is_dev): ?>
              <li class="<?=$ci->uri->segment(1) == 'task' ? 'active' : ''?>"><a href="<?=site_url('task')?>">Task</a></li>
            <?php endif ?>
            <?php if ($me->is_admin): ?>
              <li class="<?=strpos($ci->uri->uri_string(), 'admin/usermgmt') !== false ? 'active' : ''?>"><a href="<?=site_url('admin/usermgmt')?>">User</a></li>
            <?php endif ?>
          </ul>
          <!-- <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form> -->
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            
            <li class="dropdown messages-menu" id="navProjects">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Project
              </a>
              <ul class="dropdown-menu">
                <li>
                    <ul class="menu">
                        <li><a href="<?=site_url('project')?>">All projects</a></li>
                        <?php foreach(models\Project::orderBy('created_at', 'DESC')->limit(10)->get() as $project): ?>
                            <li>
                                <a href="<?=site_url('task')?>?project=<?=$project->id?>">
                                    <h4><?=$project->name?></h4>
                                    <p><?=$project->created_at->format('j-n-Y')?></p>
                                </a>
                            </li>
                        <?php endforeach ?>
                        <?php if (models\Project::count() > 10): ?>
                          <li><a href="<?=site_url('project')?>">See more..</a></li>
                        <?php endif ?>
                    </ul>
                </li>
                
              </ul>
            </li>

            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <i class="fa fa-user"></i>
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?=$me->fullname?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <p><?=$me->fullname?><br><small><?=implode(', ', $me->all_auth)?></small></p>
                </li>
                <!-- Menu Body -->
                
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?=site_url('auth/logout')?>" class="btn btn-default btn-flat">Logout</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1 style="display:inline; margin-right:10px;">
              <?=$this['title']?>
              
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="#">Layout</a></li>
              <li class="active">Top Navigation</li>
            </ol> -->
          </section>

          <!-- Main content -->
          <section class="content">
            <?=$this['content']?>
          </section>
          <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
<?=$this['script']?>

</body>
</html>
