<?php $ci =& get_instance() ?>
<?php $this->extend('layout') ?>
<?php $this->block('title', "Users") ?>

<?php $this->block('content') ?>
<div id="content" class="row">
	<div :class="showForm ? 'col-md-8' : 'col-md-10'">
		<div class="box">
			<div class="box-header with-border">
				<button type="button" class="btn btn-sm btn-primary" @click="form()">Add</button>

				<div class="pull-right">
					<?php foreach(models\User::ALL_AUTH as $field => $title): ?>
						<label class="checkbox-inline"><input type="checkbox" value="<?=$title?>" @change="refreshList" v-model="filter.<?=$field?>"> <?=$title?></label>
					<?php endforeach ?>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped" id="list">
					<thead>
						<tr><th class="text-muted" style="width: 1px;">#</th><th>Name</th><th>Username</th><th>Auth</th><th style="width: 1px;"></th></tr>
					</thead>
					<tbody>
						<tr v-for="(user,i) in users">
							<td class="text-muted">{{ i+1 }}</td>
							<td>{{ user.fullname }}</td>
							<td>{{ user.username }}</td>
							<td>{{ user.all_auth.join(', ') }}</td>
							<td>
								<a href="javascript:;" v-if="user.id != <?=$ci->session->auth->id?>" @click="form(user.id)">Edit</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4" v-if="showForm">
		<form id="formUser" method="post" :action="formAction" class="box box-success">
			<div class="box-header with-border">
				<h4 class="box-title" v-html="formTitle"></h4>
			</div>
			<div class="box-body">
				<div :class="classFullname">
					<label>Name</label>
					<input type="text" name="fullname" class="form-control" v-model="formData.fullname">
					<span class="help-block">{{ formValidation.fullname }}</span>
				</div>
				<div :class="classUsername">
					<label>Username</label>
					<input type="text" name="username" class="form-control" v-model="formData.username">
					<span class="help-block">{{ formValidation.username }}</span>
				</div>
				<div :class="classPassword">
					<label>Password</label>
					<input type="password" name="password" class="form-control" v-model="formData.password">
					<span class="help-block">{{ formValidation.password }}</span>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label><input type="checkbox" name="is_dev" v-model="formData.is_dev"> Developer</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" name="is_client" v-model="formData.is_client"> Client</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" name="is_admin" v-model="formData.is_admin"> Admin</label>
					</div>
				</div>
				<div class="text-danger" v-if="formErr" v-html="formErr"></div>
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-primary" @click="save()">Save</button>
				<button type="button" class="btn btn-default pull-right" @click="closeForm()">Close</button>
			</div>
		</form>
	</div>
</div>
<?php $this->endblock() ?>

<?php $this->block('script') ?>
<script type="text/javascript" src="<?=base_url('assets/vue.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/jquery.form.min.js')?>"></script>
<script type="text/javascript">
	new Vue({
		el: '#content',
		data: {
			users: [],
			filter: {},
			showForm: false,
			formData: {},
			formValidation: {},
			formErr: null
		},
		computed: {
			formTitle: function() {
				return this.formData.id ? 'Edit user <small>' + this.formData.fullname + '</small>' : 'New user';
			},
			formAction: function() {
				return this.formData.id ? '<?=site_url('admin/usermgmt/updateUser')?>/' + this.formData.id : '<?=site_url('admin/usermgmt/addUser')?>';
			},
			classFullname: function() {
				return 'form-group' + (this.formValidation.fullname ? ' has-error' : '');
			},
			classUsername: function() {
				return 'form-group' + (this.formValidation.username ? ' has-error' : '');
			},
			classPassword: function() {
				return 'form-group' + (this.formValidation.password ? ' has-error' : '');
			}
		},
		methods: {
			refreshList: function() {
				var payload = {auth: ''},
					filterAuth = [];
				<?php foreach (models\User::ALL_AUTH as $field => $title): ?>
					if (this.filter.<?=$field?>)
						filterAuth.push('<?=$field?>');
				<?php endforeach ?>
				payload.auth = filterAuth.join(',');
				console.log(filterAuth, payload.auth)
				$.getJSON('', payload, function(res) {
					this.users = res;
				}.bind(this));
			},
			form: function(id) {
				this.formData = {id: id};
				if (id) {
					this.showForm = true;
					$.getJSON('user/' + id, function(res) {
						this.formData = res;
					}.bind(this));
				}
				else {
					this.formData = {};
					this.showForm = true;
				}
			},
			save: function() {
				this.formValidation = {};
				this.formErr = null;
				$('#formUser').ajaxSubmit({
					success: function(res) {
						this.closeForm();
						this.refreshList();
					}.bind(this),
					error: function(xhr, status, statusText) {
						if (xhr.responseJSON) {
							this.formValidation = xhr.responseJSON;
						}
						else {
							this.formErr = 'Error: ' + xhr.status + ' ' + statusText;
						}
						console.log(arguments);
					}.bind(this)
				});
			},
			closeForm: function() {
				this.formData = {};
				this.formValidation = {};
				this.formErr = null;
				this.showForm = false;
			}
		},
		created: function() {
			this.refreshList();
		}
	});
</script>
<?php $this->endblock() ?>